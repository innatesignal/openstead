<?php

namespace Tests;

use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

// Test
abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;
}
