build:
	./dev-scripts/dev_build.sh

up:
	./dev-scripts/dev_up.sh

down:
	./dev-scripts/dev_down.sh

connect:
	./dev-scripts/dev_mount_container.sh

watch:
	./dev-scripts/dev_run_watch.sh

optimize:
	./dev-scripts/dev_optimize.sh

queue:
	./dev-scripts/dev_queue.sh

autoload:
	./dev-scripts/dev_autoload.sh

stripe-webhooks:
	./dev-scripts/dev_stripe_webhooks.sh

docker-cleanup:
	docker system prune -a

db-pull:
	./dev-scripts/dev_pull_db.sh

db-clone:
	./dev-scripts/dev_clone_db.sh

composer-install:
	COMPOSER=composer-staging.json composer install

composer-update:
	COMPOSER=composer-staging.json composer update

composer-autoload:
	COMPOSER=composer-staging.json composer dump-autoload

aws-login:
	aws ecr get-login-password --region us-east-2 | docker login --username AWS --password-stdin 044193311389.dkr.ecr.us-east-2.amazonaws.com
