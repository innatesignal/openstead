# Features

Start off by making this a totally self-hosted app

Potentially in the future, make it a hosted app with the ability to pass data to an API with a self-hostable component (for local sensor data and things like that)

## Livestock
-  Types
	- Poultry
	- Cattle (Bovine)
	- Goats (Caprine)
	- Sheep (Ovine)
	- Pigs (Porcine)
	- Rabbits
	- Horses (Equine)
	- Fish
	- Bees
	- Worms
- Enclosures (homes)
	- Repairs needed
	- Maintenance schedules
	- Camera feeds
- Health tracking
	- Medication schedules
	- Vet scheduling
    	- Vet lookup
  	- Weight
- Feeding schedules and inventory
- Location Tracking
- Product tracking
	- Eggs
	- Wool
	- Milk
	- Meat
	- etc
- Create traits for common attributes (HasLocationTracking, HasProducts, HasWeightTracking, HasEnclosure, HasMedication)

## Garden
- Beds
	- Raised
	- In ground
		- Weeding schedules
		- Flood plane lookup
		- Soil health
			- PH
			- Nutrients
			- Suggest additives for various issues (lime for acidity, etc)
	- Watering schedules
- Pest control
	- Sunlight tracking
	- Moisture tracking
	- Soil health
- Tracking
	- Growth tracking
		- Manual photo upload
		- Automated drone photography (sell hardware)

## Orchard
- Trees
- Fruit tracking
- Pest control
- Soil health

## Sales
- Revenue tracking
- Inventory on hand
- Pricing
- etc
- Maybe some sort of ecommerce site hosting

## Equipment

- Tractors
	- Maintenance schedules
	- Attachments
	- Mechanic/dealer locator

## General Permaculture
- Homestead design
	- Aerial view with zone creation
	- Zone types
		- Positive/negative interactions between different zones
			- Example: bees near garden is positive for pollination
			- Example: goats near garden potentially bad because of crop damage from grazing
- Weather tracking
	- Rainfall
	- Temperature
	- Sunlight
	- Snow/ice

- Task/Project management
  - Projects
    - Vision board
    - Kanban
  - Tasks
    - Assignable to project, optional
	- Recurring
    - One-time
  - Assignees
- Master calendar for all events and scheduled tasks
